﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesafioPOO
{
    public partial class Ejercicio3 : Form
    {
        public Ejercicio3()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int num1, num2;
            int total = 0;

            int ValorMaximo = 0;
            int ValorMinimo = 0;

            //FOR EACH para verificar si la suma de los datos del ListBox
            foreach (var item in listNum.Items)
            {
                //string que extrae los valores del listbox sin espacios
                total += Convert.ToInt32(item);
            }

            //si la SUMA es igual o mayor a 200 se borran los datos y se piden de nuevo
            if (total >= 200)
            {
                txtNum.Enabled = true;
                txtNum.Clear();
                lblAviso.Text = "¡La suma de datos mayor a 200, ingrese otros!";
                listNum.Items.Clear();
            }






            //Se recorre cada dato del listBox
            for (int i = 0; i < listNum.Items.Count; i++)
            {   
                //Inició el indice en cero
                if (i == 0)
                {
                    ValorMaximo = int.Parse(listNum.Items[i].ToString());
                    ValorMinimo = int.Parse(listNum.Items[i].ToString());
                }
                else
                {
                    //el valor actual que estará cambiando al recorrer cada num del listBox
                    int ValorActual = int.Parse(listNum.Items[i].ToString());


                    //si el numero en ValorActual es mayor al encontrado en la posición 0 yo iguala y repite

                    //MAYOR                                        
                    if (ValorActual > ValorMaximo)
                    {
                        ValorMaximo = ValorActual;
                    }


                    //si el numero en ValorActual es menor al encontrado en la posición 0 yo iguala y repite

                    //MENOR                   
                    if (ValorActual < ValorMinimo)
                    {
                        ValorMinimo = ValorActual;
                    }


                    //Condiciones del Desafio
                    if (ValorMinimo > 10)
                    {
                        txtMayor.Text = (Convert.ToInt32(ValorMaximo) + 10).ToString();
                        lblAviso.Text = "-";
                    }

                    if (ValorMaximo < 50)
                    {
                        txtMenor.Text = (Convert.ToInt32(ValorMinimo) - 5).ToString();
                        lblAviso.Text = "-";
                    }

                }              
                            
            }




           
        }
                

        private void txtNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            string verificadorNumero = "";
            string nums = "";
            int total = 0;
            

            //al presionar el enter añade lo del textbox al listbox
            if (e.KeyChar == (int)Keys.Enter)
            {
                Regex regex = new Regex(@"[0-9]+$");

                //Validando que solo es número entero con expresión regular
                if (!regex.IsMatch(txtNum.Text))
                {
                    lblAviso.Text = "¡Solo permiten números!";
                }

                //Si la cantidad de datos del listBox es menor a 4 permite ingresar
                if(listNum.Items.Count < 4)
                {
                    //validación que no este vacío
                    if (txtNum.Text != "")
                    {
                        //For para verificar que los datos no se repitan
                        for (int i = 0; i < listNum.Items.Count; i++)
                        {
                            nums = txtNum.Text;
                            verificadorNumero = listNum.Items[i].ToString();

                            //En caso que si, muestra aviso y limpia el txt
                            if (verificadorNumero == nums)
                            {
                                lblAviso.Text = "No se pueden repetir los números";
                                txtNum.Clear();
                                return;
                            }
                        }

                        //Si el número es igual a cero no permitir seguir
                        if (Convert.ToInt32(txtNum.Text) == 0)
                        {                         
                            lblAviso.Text = "No se permite el cero";
                            txtNum.Clear();
                            return;
                        }

                        //SI pasa todo Ingresa los números
                        listNum.Items.Add(txtNum.Text);
                        txtNum.Clear();
                    }
                    else
                    {
                        lblAviso.Text = "No se permiten campos vacíos";
                        txtNum.Clear();
                        return;
                    }
                }
                else
                {
                    txtNum.Enabled = false;
                    lblAviso.Text = "¡Máximo de datos alcanzados!";

                    //FOR EACH para verificar si la suma de los datos del ListBox
                    foreach (var item in listNum.Items)
                    {
                        //string que extrae los valores del listbox sin espacios
                        total += Convert.ToInt32(item);
                    }
                    //si la SUMA es igual o mayor a 200 se borran los datos y se piden de nuevo
                    if (total >= 200)
                    {
                        txtNum.Enabled = true;
                        txtNum.Clear();
                        lblAviso.Text = "¡La suma de datos mayor a 200, ingrese otros!";
                        listNum.Items.Clear();
                    }                    
                }
            }
            else
            {
                lblAviso.Text = "¡Presiona enter para ingresar los datos!";
            }


           

        }
    }
}
