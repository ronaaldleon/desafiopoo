﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesafioPOO
{
    public partial class Ejer4 : Form
    {
        public Ejer4()
        {
            InitializeComponent();
        }

        //se trabajará con una matriz de 3x4 para almacenar los datos, a continuación un bosquejo de la matriz

        /* 
         * 
         * 
         _                              _
        |                                |
        |nombre1,  apellido, cargo, horas|
        |nombre2,  apellido, cargo, horas|
        |nombre3,  apellido, cargo, horas|        
        |_                              _|



         */

        //metodo unicamente para hacer pruebas
        public void Imprimir()
        {
            for (int f = 0; f < 3; f++)
            {
                for (int c = 0; c < 4; c++)
                {
                    MessageBox.Show(matrizEmpleados[f, c] + "\t");
                    Console.Write("\t" + matrizEmpleados[f, c] + "\t");

                }

            }

        }

        //metodo para imprimir resultdos finales
        public void Mostrar()
        {
            //se imprimen los nombres anidados
            textBox1.Text = matrizEmpleados[0, 0] + " " + matrizEmpleados[0, 1];
            textBox2.Text = matrizEmpleados[1, 0] + " " + matrizEmpleados[1, 1];
            textBox3.Text = matrizEmpleados[2, 0] + " " + matrizEmpleados[2, 1];
        }

        //funcion para calcular sueldo base sin bono
        public double calcularSueldoBase(double horas)
        {
            double sueldoBase = 0;
            //condicion de horas para el calculo            
            if (horas <= 160)
            {
                sueldoBase = Math.Round((horas * 9.75), 2);
            }
            else
            {
                //se calculan las primeras 160 h
                sueldoBase = Math.Round((horas * 9.75), 2);
                //luego se saca la diferencia entre horas
                horas = Math.Abs(horas - 160);
                sueldoBase = sueldoBase + (horas * 11.50);
            }

            return sueldoBase;
        }
        //sueldo base CON BONO
        public double calcularSueldoBaseBono(double sueldo, string cargo)
        {
            double sueldoBase = 0;
            switch (cargo)
            {
                case "Gerente":
                    sueldoBase = (sueldo * 0.10) + sueldo;
                    break;

                case "Asistente":
                    sueldoBase = (sueldo * 0.05) + sueldo;
                    break;

                case "Secretaria":
                    sueldoBase = (sueldo * 0.03) + sueldo;
                    break;
            }
            return sueldoBase;
        }

        //funcion par calcular sueldo liquido
        public double calcularSueldoLiquido(double sueldo, double iss, double afp, double renta)
        {
            double sueldoLiquido = 0;
            /*
             Se le descontara del ISSS el 5.25% de su sueldo base
             Se le descontara de la AFP el 6.88% de su sueldo base
             Se le descontara de la RENTA el 10% de su sueldo
             */

            sueldoLiquido = sueldo - (iss + afp + renta);

            return sueldoLiquido;
        }

        //funcion iss
        public double calculoSeguro(double sueldo)
        {
            double sueldoFinal = 0;
            sueldoFinal = (sueldo * 0.0525);
            return sueldoFinal;
        }

        //funcion afp
        public double calculoAFP(double sueldo)
        {
            double sueldoFinal = 0;
            sueldoFinal = (sueldo * 0.0688);
            return sueldoFinal;
        }

        //funcion renta
        public double calculoRenta(double sueldo)
        {
            double sueldoFinal = 0;
            sueldoFinal = (sueldo * 0.10);
            return sueldoFinal;
        }

        //matriz        
        public string[,] matrizEmpleados = new string[3, 4];



        int intentos = 0;
        bool validarTxtVacio(Form formulario) //instancia de tipo form que ayudará a detectar los txt
        {
            // variable para trabajar con la funcion
            bool vacio = false;
            foreach (Control controles in formulario.Controls) // recorro los controles o elementos que contiene el form
            {
                // indicamos que sea textbox y verifico que si está vacio
                if (controles is TextBox & controles.Text == String.Empty & controles.Enabled == true)
                {
                    vacio = true;
                }
            }

            //si la variable bool es true, entonces significa que algún txt se encuentra vacío
            if (vacio == true)
            {
                MessageBox.Show("Llena los campos que estén vacios!");
                //vacio = false;

            }
            return vacio;
        }

        private void limpiarControles()
        {
            txtNombre1.Text = "";
            txtApellido.Text = "";
            txtHoras.Text = "";
        }



        private void Ejer4_Load(object sender, EventArgs e)
        {
            //con esta propiedad el usuario no puede editar las opciones del cbo
            cboCargo1.DropDownStyle = ComboBoxStyle.DropDownList;

            //lleno de opciones al  cbo

            cboCargo1.Items.Clear();
            cboCargo1.Items.Add("Gerente");
            cboCargo1.Items.Add("Asistente");
            cboCargo1.Items.Add("Secretaria");
            cboCargo1.SelectedIndex = 0; //seleccionada la opcion 1 por defecto
                                         //

            groupBox1.Enabled = false;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //mando a llamar mi funcion e validar txt's vacios
            if (!validarTxtVacio(this))
            {
                if (txtHoras.Text == "0")
                {
                    MessageBox.Show("Ingresa un valor diferente de cero!");
                    txtHoras.Focus();
                    txtHoras.Text = "";
                }
                else
                {
                    //variables a utilizar
                    string nombres, apellidos, cargo, horas;
                    //capturo la informacion ingresada y las guardo en variables
                    nombres = txtNombre1.Text;
                    apellidos = txtApellido.Text;
                    cargo = cboCargo1.SelectedItem.ToString();
                    horas = txtHoras.Text;
                    //la variable intentos sirve como una variable iteradora,
                    //esto permite llevar un control de cada registro

                    matrizEmpleados[intentos, 0] = nombres;
                    matrizEmpleados[intentos, 1] = apellidos;
                    matrizEmpleados[intentos, 2] = cargo;
                    matrizEmpleados[intentos, 3] = horas;

                    limpiarControles();
                    intentos++;//voy incrementando la iteradora


                    //verifico cuando se hayan realizado 3 intentos, si es así deshabilito el botón y limpio el form
                    if (intentos == 3)
                    {
                        btnGuardar.Enabled = false;
                        MessageBox.Show("Limite de registros permitidos alcanzados");
                        limpiarControles();
                        button1.Enabled = true;
                    }

                }
            }
        }

        private void txtHoras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            //solo permito que se ingrese un punto decimal
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Imprimir();
            Mostrar();
            groupBox1.Enabled = true;
            //vamos a verificar si la sucesión de roles guardados es igual a gerente-asistente-secretaria
            //al trabajar con matriz, se hace más facil a la hora de acceder a cualquiera de sus indices
            //simplemente se indica la posición
            if (matrizEmpleados[0, 2] == "Gerente" && matrizEmpleados[1, 2] == "Asistente" && matrizEmpleados[2, 2] == "Secretaria")
            {
                //NO HAY BONO
                radioButton2.Checked = true;
                //calcularSueldoBase(Convert.ToDouble(matrizEmpleados[0, 3]));//hago la conversion para solventar error
                //MessageBox.Show(matrizEmpleados[0, 3] + "");
                //ISSS 
                double e1 = calculoSeguro(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[0, 3])));//hago la conversion para solventar error
                txtISSS.Text = e1.ToString();
                double e2 = calculoSeguro(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[1, 3])));//hago la conversion para solventar error
                textBox5.Text = e2.ToString();
                double e3 = calculoSeguro(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[2, 3])));//hago la conversion para solventar error
                txtISSS3.Text = e3.ToString();

                //AFP
                double a1 = calculoAFP(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[0, 3])));//hago la conversion para solventar error
                txtAFP1.Text = a1.ToString();
                double a2 = calculoAFP(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[1, 3])));//hago la conversion para solventar error
                txtAFP2.Text = a2.ToString();
                double a3 = calculoAFP(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[2, 3])));//hago la conversion para solventar error
                txtAFP3.Text = a3.ToString();

                //RENTA
                double r1 = calculoRenta(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[0, 3])));//hago la conversion para solventar error
                txtRenta1.Text = r1.ToString();
                double r2 = calculoRenta(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[1, 3])));//hago la conversion para solventar error
                txtRenta2.Text = r2.ToString();
                double r3 = calculoRenta(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[2, 3])));//hago la conversion para solventar error
                txtRenta3.Text = r3.ToString();

                //SUELDO F
                double s1 = calcularSueldoLiquido(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[0, 3])), e1, a1, r1);
                txtSueldo1.Text = s1.ToString();
                double s2 = calcularSueldoLiquido(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[1, 3])), e2, a2, r2);
                txtSueldo2.Text = s2.ToString();
                double s3 = calcularSueldoLiquido(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[2, 3])), e3, a3, r3);
                txtSueldo3.Text = s3.ToString();

            }
            else
            {
                //SI HAY BONO
                radioButton1.Checked = true;

                double d1 = calcularSueldoBaseBono(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[0, 3])), matrizEmpleados[0, 2]);
                double d2 = calcularSueldoBaseBono(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[1, 3])), matrizEmpleados[1, 2]);
                double d3 = calcularSueldoBaseBono(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[2, 3])), matrizEmpleados[2, 2]);
                double i1 = calculoSeguro(d1);
                txtISSS.Text = i1.ToString();
                double i2 = calculoSeguro(d2);
                textBox5.Text = i2.ToString();
                double i3 = calculoSeguro(d3);
                txtISSS3.Text = i3.ToString();

                //renta
                double rr1 = calculoRenta(d1);
                txtRenta1.Text = rr1.ToString();
                double rr2 = calculoRenta(d2);
                txtRenta2.Text = rr2.ToString();
                double rr3 = calculoRenta(d3);
                txtRenta3.Text = rr3.ToString();

                //afp
                double aa1 = calculoAFP(d1);
                txtAFP1.Text = aa1.ToString();
                double aa2 = calculoAFP(d2);
                txtAFP2.Text = aa2.ToString();
                double aa3 = calculoAFP(d3);
                txtAFP3.Text = aa3.ToString();


                //sueldo F
                double ff1 = calcularSueldoLiquido(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[0, 3])), i1, aa1, rr1);
                txtSueldo1.Text = ff1.ToString();
                double ff2 = calcularSueldoLiquido(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[1, 3])), i2, aa2, rr2);
                txtSueldo2.Text = ff2.ToString();
                double ff3 = calcularSueldoLiquido(calcularSueldoBase(Convert.ToDouble(matrizEmpleados[2, 3])), i3, aa3, rr3);
                txtSueldo3.Text = ff3.ToString();
            }
        }
    }
}
