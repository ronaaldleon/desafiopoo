﻿
namespace DesafioPOO
{
    partial class Ejer4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRenta3 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtRenta2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtRenta1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtAFP3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtAFP2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtAFP1 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSueldo3 = new System.Windows.Forms.TextBox();
            this.txtSueldo2 = new System.Windows.Forms.TextBox();
            this.txtSueldo1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtISSS3 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.txtISSS = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtHoras = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.cboCargo1 = new System.Windows.Forms.ComboBox();
            this.txtNombre1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRenta3
            // 
            this.txtRenta3.Enabled = false;
            this.txtRenta3.Location = new System.Drawing.Point(946, 590);
            this.txtRenta3.Multiline = true;
            this.txtRenta3.Name = "txtRenta3";
            this.txtRenta3.Size = new System.Drawing.Size(166, 34);
            this.txtRenta3.TabIndex = 108;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(874, 587);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 13);
            this.label20.TabIndex = 107;
            this.label20.Text = "RENTA";
            // 
            // txtRenta2
            // 
            this.txtRenta2.Enabled = false;
            this.txtRenta2.Location = new System.Drawing.Point(946, 522);
            this.txtRenta2.Multiline = true;
            this.txtRenta2.Name = "txtRenta2";
            this.txtRenta2.Size = new System.Drawing.Size(166, 34);
            this.txtRenta2.TabIndex = 106;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(874, 519);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 13);
            this.label21.TabIndex = 105;
            this.label21.Text = "RENTA";
            // 
            // txtRenta1
            // 
            this.txtRenta1.Enabled = false;
            this.txtRenta1.Location = new System.Drawing.Point(946, 463);
            this.txtRenta1.Multiline = true;
            this.txtRenta1.Name = "txtRenta1";
            this.txtRenta1.Size = new System.Drawing.Size(166, 34);
            this.txtRenta1.TabIndex = 104;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(874, 460);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 13);
            this.label22.TabIndex = 103;
            this.label22.Text = "RENTA";
            // 
            // txtAFP3
            // 
            this.txtAFP3.Enabled = false;
            this.txtAFP3.Location = new System.Drawing.Point(698, 577);
            this.txtAFP3.Multiline = true;
            this.txtAFP3.Name = "txtAFP3";
            this.txtAFP3.Size = new System.Drawing.Size(166, 34);
            this.txtAFP3.TabIndex = 102;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(626, 574);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 13);
            this.label19.TabIndex = 101;
            this.label19.Text = "AFP";
            // 
            // txtAFP2
            // 
            this.txtAFP2.Enabled = false;
            this.txtAFP2.Location = new System.Drawing.Point(698, 509);
            this.txtAFP2.Multiline = true;
            this.txtAFP2.Name = "txtAFP2";
            this.txtAFP2.Size = new System.Drawing.Size(166, 34);
            this.txtAFP2.TabIndex = 100;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(626, 506);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(27, 13);
            this.label18.TabIndex = 99;
            this.label18.Text = "AFP";
            // 
            // txtAFP1
            // 
            this.txtAFP1.Enabled = false;
            this.txtAFP1.Location = new System.Drawing.Point(698, 450);
            this.txtAFP1.Multiline = true;
            this.txtAFP1.Name = "txtAFP1";
            this.txtAFP1.Size = new System.Drawing.Size(166, 34);
            this.txtAFP1.TabIndex = 98;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(626, 447);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 97;
            this.label17.Text = "AFP";
            // 
            // txtSueldo3
            // 
            this.txtSueldo3.Enabled = false;
            this.txtSueldo3.Location = new System.Drawing.Point(1122, 345);
            this.txtSueldo3.Multiline = true;
            this.txtSueldo3.Name = "txtSueldo3";
            this.txtSueldo3.Size = new System.Drawing.Size(229, 34);
            this.txtSueldo3.TabIndex = 96;
            // 
            // txtSueldo2
            // 
            this.txtSueldo2.Enabled = false;
            this.txtSueldo2.Location = new System.Drawing.Point(1122, 280);
            this.txtSueldo2.Multiline = true;
            this.txtSueldo2.Name = "txtSueldo2";
            this.txtSueldo2.Size = new System.Drawing.Size(229, 34);
            this.txtSueldo2.TabIndex = 95;
            // 
            // txtSueldo1
            // 
            this.txtSueldo1.Enabled = false;
            this.txtSueldo1.Location = new System.Drawing.Point(1122, 215);
            this.txtSueldo1.Multiline = true;
            this.txtSueldo1.Name = "txtSueldo1";
            this.txtSueldo1.Size = new System.Drawing.Size(229, 34);
            this.txtSueldo1.TabIndex = 94;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1050, 348);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 93;
            this.label14.Text = "SUELDO";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1050, 277);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 92;
            this.label15.Text = "SUELDO";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(1050, 218);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 13);
            this.label16.TabIndex = 91;
            this.label16.Text = "SUELDO";
            // 
            // txtISSS3
            // 
            this.txtISSS3.Enabled = false;
            this.txtISSS3.Location = new System.Drawing.Point(461, 571);
            this.txtISSS3.Multiline = true;
            this.txtISSS3.Name = "txtISSS3";
            this.txtISSS3.Size = new System.Drawing.Size(146, 34);
            this.txtISSS3.TabIndex = 90;
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(461, 506);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(146, 34);
            this.textBox5.TabIndex = 89;
            // 
            // txtISSS
            // 
            this.txtISSS.Enabled = false;
            this.txtISSS.Location = new System.Drawing.Point(461, 441);
            this.txtISSS.Multiline = true;
            this.txtISSS.Name = "txtISSS";
            this.txtISSS.Size = new System.Drawing.Size(146, 34);
            this.txtISSS.TabIndex = 88;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(389, 574);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 87;
            this.label13.Text = "iss\r\n";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(389, 503);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 13);
            this.label12.TabIndex = 86;
            this.label12.Text = "ISS";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(389, 447);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 13);
            this.label11.TabIndex = 85;
            this.label11.Text = "ISS";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(142, 338);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(481, 78);
            this.groupBox1.TabIndex = 84;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bono";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.Color.Lime;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(171, 34);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(41, 22);
            this.radioButton1.TabIndex = 39;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Sí";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.Red;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(335, 34);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(48, 22);
            this.radioButton2.TabIndex = 40;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "No";
            this.radioButton2.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "¿Poseedores de bono?";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(405, 308);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 83;
            this.label8.Text = "Zona de resultados";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 577);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 82;
            this.label7.Text = "Nombres emp3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 506);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 81;
            this.label6.Text = "Nombres emp2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 444);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 80;
            this.label5.Text = "Nombres emp1";
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(131, 574);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(229, 34);
            this.textBox3.TabIndex = 79;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(131, 503);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(229, 34);
            this.textBox2.TabIndex = 78;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(131, 441);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(229, 34);
            this.textBox1.TabIndex = 77;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(503, 163);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(219, 43);
            this.button1.TabIndex = 76;
            this.button1.Text = "Mostrar resultados";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtHoras
            // 
            this.txtHoras.Location = new System.Drawing.Point(161, 218);
            this.txtHoras.Multiline = true;
            this.txtHoras.Name = "txtHoras";
            this.txtHoras.Size = new System.Drawing.Size(269, 32);
            this.txtHoras.TabIndex = 75;
            this.txtHoras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHoras_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(66, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 74;
            this.label4.Text = "Horas:";
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(161, 107);
            this.txtApellido.Multiline = true;
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(269, 32);
            this.txtApellido.TabIndex = 73;
            // 
            // cboCargo1
            // 
            this.cboCargo1.FormattingEnabled = true;
            this.cboCargo1.Location = new System.Drawing.Point(161, 168);
            this.cboCargo1.Name = "cboCargo1";
            this.cboCargo1.Size = new System.Drawing.Size(269, 21);
            this.cboCargo1.TabIndex = 72;
            // 
            // txtNombre1
            // 
            this.txtNombre1.Location = new System.Drawing.Point(161, 48);
            this.txtNombre1.Multiline = true;
            this.txtNombre1.Name = "txtNombre1";
            this.txtNombre1.Size = new System.Drawing.Size(269, 32);
            this.txtNombre1.TabIndex = 71;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 70;
            this.label3.Text = "Cargo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 69;
            this.label2.Text = "Apellidos:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "Nombres:";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(503, 94);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(219, 38);
            this.btnGuardar.TabIndex = 67;
            this.btnGuardar.Text = "Guardar datos";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // Ejer4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 673);
            this.Controls.Add(this.txtRenta3);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtRenta2);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtRenta1);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtAFP3);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtAFP2);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtAFP1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtSueldo3);
            this.Controls.Add(this.txtSueldo2);
            this.Controls.Add(this.txtSueldo1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtISSS3);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.txtISSS);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtHoras);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtApellido);
            this.Controls.Add(this.cboCargo1);
            this.Controls.Add(this.txtNombre1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGuardar);
            this.Name = "Ejer4";
            this.Text = "Ejer4";
            this.Load += new System.EventHandler(this.Ejer4_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRenta3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtRenta2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtRenta1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtAFP3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtAFP2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtAFP1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSueldo3;
        private System.Windows.Forms.TextBox txtSueldo2;
        private System.Windows.Forms.TextBox txtSueldo1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtISSS3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox txtISSS;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtHoras;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.ComboBox cboCargo1;
        private System.Windows.Forms.TextBox txtNombre1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGuardar;
    }
}